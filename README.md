# Cx-Oralce with Newrelic #


### How do I get set up? ###

* Install cx-Orale https://oracle.github.io/odpi/doc/installation.html#linux
* Install requirements 
```
pip install -r requirements.txt
```
* Execute start.sh
```
chmod +x ./start.sh
./start <New Relic Key>
```
