#! /usr/bin/python

from sqlalchemy import create_engine, pool
import logging
import json


query_cartoes = 'call PCK_MEIO_DE_PAGAMENTO.CONSULTAR_TODOS_CARTOES(:P_CELULAR, :P_CPF, :O_CURSOR)'

cstr = 'oracle+cx_oracle://system:oracle@(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = ec2-52-42-254-182.us-west-2.compute.amazonaws.com)(PORT = 49161))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = xe)))'

engine = create_engine(
        cstr,
        convert_unicode=False,
        pool_size=20,
        max_overflow=0,
        echo=True,
        poolclass=pool.QueuePool
    )


def buscar_todos_os_cartoes(celular, cpf):
    logger = logging.getLogger(__name__)

    oracle_conn = engine.raw_connection()
    in_cursor = oracle_conn.connection.cursor()
    out_cursor = oracle_conn.connection.cursor()

    logger.info('cpf={} mensagem={}'.format(cpf,json.dumps({'P_CELULAR': celular,'P_CPF': cpf}, ensure_ascii=False)))

    in_cursor.execute(query_cartoes,P_CELULAR=celular, P_CPF=cpf,O_CURSOR=out_cursor )
    rows = out_cursor.fetchall()

    logger.info('cpf={} mensagem={}'.format(cpf,json.dumps(rows,ensure_ascii=False)))

    cartoes = []
    for row in rows:
        cartao = {
            'sufixoCartao': row[1],
            'bandeira': row[2],
            'id': row[0]
        }
        cartoes.append(cartao)
    in_cursor.close()
    out_cursor.close()
    oracle_conn.close()
    return cartoes


if __name__ == '__main__':
    print(buscar_todos_os_cartoes(celular= '21988885566', cpf = '11111111030'))